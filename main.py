from game_managers import GameManager
from random import seed


def main():
    seed(10)
    manager = GameManager(3)
    manager.game_loop()
    pass


if __name__ == '__main__':
    main()
