from enum import IntEnum, auto


class CardSuit(IntEnum):
    HEARTS = auto()  # Червы
    DIAMONDS = auto()  # Бубны
    CLUBS = auto()  # Трефы
    SPADES = auto()  # Пики

    def __str__(self):
        return self._name_


class CardValue(IntEnum):
    # TWO = auto()
    # THREE = auto()
    # FOUR = auto()
    # FIVE = auto()
    SIX = auto()
    SEVEN = auto()
    EIGHT = auto()
    NINE = auto()
    TEN = auto()
    JACK = auto()
    QUEEN = auto()
    KING = auto()
    ACE = auto()


class Card:
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def __str__(self):
        return f'({self.value._name_}, {self.suit._name_})'

    __repr__ = __str__
