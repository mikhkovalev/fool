from random import shuffle
from itertools import islice, cycle, chain, repeat

from cards import Card, CardSuit, CardValue
from players import ConsolePlayer


class GameManager:
    MAX_PLAYERS_COUNT = 6
    HAND_CARDS_COUNT = 6

    def __init__(self, players_count):
        self.deck = [
            Card(suit, value)
            for suit in CardSuit
            for value in CardValue
        ]
        self.deck_size = len(self.deck)
        self.trump = None
        self.deck_top_idx = None
        self.players_count = players_count

        self.charge_deck()

        self.players = None
        self.active_players = None
        self.next_player_idx = None
        self.create_players()
        self.hand_around()

    def duel(self, attacker, defender):
        """
        Возвращает True, если defender успешно
        отбился
        """
        battles = []
        print('\n\n')
        while True:
            # TODO: Предусмотреть вариант,
            # что хотя бы один раз атакующий
            # должен сходить
            # TODO: Предусмотреть правило
            # типа "первый отбой 5 карт"?
            attacking = (
                    bool(defender.hand) and
                    attacker.attack(
                        battles, self.trump.suit)
            )
            if not attacking:
                return True

            defended = defender.defend(
                battles, self.trump.suit)
            if not defended:
                defender.hand.extend(filter(
                    None, chain.from_iterable(battles)))
                return False

    def take_cards(self, player):
        if self.deck_top_idx >= self.deck_size:
            return
        count_to_take = (self.HAND_CARDS_COUNT -
                         len(player.hand))
        new_deck_top_idx = self.deck_top_idx + count_to_take
        deck_slice = islice(
            self.deck,
            self.deck_top_idx,
            new_deck_top_idx
        )
        player.hand.extend(deck_slice)
        self.deck_top_idx = min(
            new_deck_top_idx, self.deck_size)

    def game_loop(self):
        attacker = self.get_next_player()
        defender = self.get_next_player()

        while True:
            # TODO: Если остался только один
            # активный игрок -- игра закончена,
            # оставшийся -- проиграл
            defended = self.duel(attacker, defender)
            if defended:
                self.take_cards(defender)
                self.take_cards(attacker)
                attacker = defender
                defender = self.get_next_player()
            else:
                self.take_cards(attacker)
                attacker = self.get_next_player()
                defender = self.get_next_player()

            # Если по окончании хода у каких-то
            # игроков пустая рука (действие
            # происходит после взятия из прикупа),
            # значит эти игроки выбывают.
            for idx, player in enumerate(self.players):
                if not player.hand:
                    self.active_players[idx] = False

    def charge_deck(self):
        """
        Вы их в киосках что-ли заряжаете?! (с)
        """
        shuffle(self.deck)

    def create_players(self):
        self.players = tuple(
            ConsolePlayer(str(i))
            for i in range(self.players_count)
        )
        self.active_players = list(repeat(
            True, self.players_count))
        self.next_player_idx = 0

    def increase_next_player_idx(self):
        self.next_player_idx = (
            (self.next_player_idx + 1) % self.players_count)

    def get_next_player(self):
        while not self.active_players[
                self.next_player_idx]:
            self.increase_next_player_idx()
        next_player = self.players[self.next_player_idx]
        self.increase_next_player_idx()
        return next_player

    def hand_around(self):
        """
        Метод раздачи карт. Заодно запоминает козырь
        """
        trump_idx = self.players_count * self.HAND_CARDS_COUNT

        # Этот же индекс будет у верхней карты в
        # прикупе
        self.deck_top_idx = trump_idx
        try:
            # Достаём козырь после всех разданных
            # карт и помещаем его в конец колоды
            self.trump = self.deck[trump_idx]
            self.deck.pop(trump_idx)
            self.deck.append(self.trump)
        except IndexError:
            # Были разданы все карты, значит
            # козырь -- это последняя карта в
            # колоде
            self.trump = self.deck[-1]

        for idx, player in enumerate(self.players):
            player.hand.extend(islice(
                self.deck,
                idx,
                trump_idx,
                self.players_count
            ))
