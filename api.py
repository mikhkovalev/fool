class Battle:
    def __init__(self, lunge):
        self.lunge = lunge
        self.parry = None
        self.__idx = 0
    
    def can_beat(self, parry_candidate, trump_suit):
        result = False

        if self.lunge.suit == parry_candidate.suit:
            result = self.lunge.value < parry_candidate.value
        elif parry_candidate.suit == trump_suit:
            result = True

        return result

    def __iter__(self):
        return self

    def __next__(self):
        if self.__idx == 0:
            result = self.lunge
        elif self.__idx == 1:
            result = self.parry
        else:
            self.__idx = 0
            raise StopIteration
        self.__idx += 1
        return result


def card_sort_key_getter(trump_suit):
    def card_sort_key(card):
        return card.suit == trump_suit, card.value

    return card_sort_key
