import os
from itertools import chain

from api import Battle, card_sort_key_getter


class Player:
    def __init__(self, name):
        self.hand = []
        self.name = name

    def attack(self, battles, trump_suit):
        raise NotImplementedError

    def defend(self, battles, trump_suit):
        raise NotImplementedError

    def get_cards_for_attack(self, battles, trump_suit):
        # Если ни одного боя не было -- ходим
        # любой имеющейся картой
        if not battles:
            # Возможно следует возвращать копию...
            return self.hand

        # Иначе можем использовать только карты тех
        # достоинств, что уже были в бою
        usable_values = {
            card.value
            for card in filter(None, chain.from_iterable(battles))
        }
        usable_cards = [
            card for card in self.hand
            if card.value in usable_values
        ]

        return usable_cards

    @staticmethod
    def can_attack(card, battles):
        # Если ни одного боя не было -- ходим
        # любой имеющейся картой
        if not battles:
            return True

        # Иначе можем использовать только карты тех
        # достоинств, что уже были в бою
        return any(
            card.value == used_card.value
            for used_card in chain.from_iterable(battles)
            if used_card
        )

    def get_cards_for_defend(self, active_battle, trump_suit):
        usable_cards = [
            # Можем использовать только те карты,
            # которые могут отбить текущий выпад
            card for card in self.hand
            if active_battle.can_beat(card, trump_suit)
        ]
        return usable_cards


class ConsolePlayer(Player):
    @staticmethod
    def clear_console():
        if os.name == 'nt':
            os.system('cls')
        else:
            os.system('clean')

    @staticmethod
    def select_card(allowed_cards):
        assert isinstance(allowed_cards, list)

        chosen_idx = -1

        if not allowed_cards:
            print('You have no choice!')
            return chosen_idx

        allowed_ids = range(
            -1, len(allowed_cards))

        print('Your can use:')
        for idx, card in enumerate(allowed_cards, 1):
            print(idx, '. ', card, sep='')

        while True:
            correct = True
            try:
                chosen_idx = -1 + int(input(
                    'Enter card index or 0: '))
                if chosen_idx not in allowed_ids:
                    correct = False
            except ValueError:
                correct = False

            if correct:
                break
            print('Incorrect input!')
        return chosen_idx

    def attack(self, battles, trump_suit):
        assert isinstance(battles, list)
        self.clear_console()
        print(f'Player {self.name}!')
        print(
            f'You are attacking! '
            f'Trump is {str(trump_suit)}')
        print(
            f'Used cards: '
            f'{tuple(card for card in chain.from_iterable(battles) if card)}'
        )
        # cards_for_attack = self.get_cards_for_attack(battles, trump_suit)
        attacking = False
        while True:
            chosen_idx = self.select_card(
                self.hand)
            correct_input = chosen_idx < 0 or self.can_attack(
                self.hand[chosen_idx], battles)
            if correct_input:
                break
            print('You can\'t use this card!')
        if chosen_idx >= 0:
            lunge = self.hand[chosen_idx]
            battles.append(Battle(lunge))
            self.hand.remove(lunge)
            attacking = True
        return attacking

    def defend(self, battles, trump_suit):
        assert isinstance(battles, list)
        assert battles
        active_battle = battles[-1]
        assert isinstance(active_battle, Battle)
        self.clear_console()
        print(f'Player {self.name}!')
        print(f'You have been attacked by '
              f'{active_battle.lunge}')
        print(f'Trump is {str(trump_suit)}')
        print(f'Used cards: {tuple(card for card in chain.from_iterable(battles) if card)}')
        defended = False
        while True:
            chosen_idx = self.select_card(
                self.hand)
            correct_input = chosen_idx < 0 or active_battle.can_beat(
                self.hand[chosen_idx], trump_suit)
            if correct_input:
                break
            print('You can\'t use this card!')

        if chosen_idx >= 0:
            parry = self.hand[chosen_idx]
            active_battle.parry = parry
            self.hand.remove(parry)
            defended = True
        return defended


class SimpleComputerPlayer(Player):
    pass
